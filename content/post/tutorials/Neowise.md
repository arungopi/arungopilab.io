---
title: "Comet Neowise (C/2020 F3)"
date: 2020-07-17T12:00:04+05:30
description: "മൊബൈലിൽ എങ്ങനെ കോമെറ്റ് NEOWISE നെ കാണാം "
draft: false
show_toc: true
tags: ["astronomy"]
series:
-
categories:
- tutorials
image: images/feature2/astro.png
---
## Neowise

അടുത്ത 6800 വർഷത്തേക്ക് നമ്മുടെ അടുത്ത് വരാൻ സാധ്യത ഇല്ലാത്ത എന്നാൽ ഇപ്പോൾ കാണാൻ കഴിയുന്നതുമായ ഒരു കോമെറ്റ് ആണ് C/2020 F3 അഥവാ **Neowise**. അതായത് ഇപ്പൊ കണ്ടില്ലേൽ പിന്നെ ഇല്ല. ഉത്തരധ്രുവത്തിൽ മാത്രമേ ഈ കൊമെറ്റിനെ കാണാൻ കഴിയുകയുള്ളൂ . 

> Comet NEOWISE was first discovered in March by the infrared-optimized NEOWISE spacecraft (the name is short for Near-Earth Object Wide-field Infrared Space Explorer)[^1].


കൂടുതൽ അറിയാം : [explainedinmalayalam.com](https://www.explainedinmalayalam.com/2020/07/neowise-comet.html)
 
### എപ്പോൾ, എവിടെ കാണാം ?
ജൂലൈ 14 മുതൽ, 20 ദിവസം സൂര്യൻ അസ്തമിച്ചു ഒരു ഇരുപത് മിനുട്ട് നമുക്ക് നഗ്ന നേത്രം കൊണ്ട് തന്നെ കാണാനാകും. കൃത്യമായി പറഞ്ഞാൽ വടക്കു പടിഞ്ഞാറ് ദിശയിൽ. ഒരു ബൈനോക്കുലറൊ ടെലെസ്കൊപ്പൊ ഉണ്ടേൽ ഉഷാർ.  

### എങ്ങനെ കാണാം ?

കേരളത്തിൽ ഇപ്പോൾ മൺസൂൺ കാലമായതിനാൽ ഈ സുന്ദരനെ കാണുക പ്രയാസം തന്നെ . ഒപ്പം covid കൂടി ഉള്ളതിനാൽ എല്ലാപേർക്കും ഈ അവസരം കിട്ടണം എന്നില്ല. അപ്പോൾ നമ്മുടെ സഹായത്തിന് mobile നെ ആശ്രയിക്കാം . Play store -ൽ ലഭ്യമായ ഒരു ആപ്പ് ആണ്  [Sky safari](https://play.google.com/store/apps/details?id=com.simulationcurriculum.skysafari5&hl=en_US).  

## കാണാം Sky safari ആപ്പിൽ

ആദ്യം ചെയ്യണ്ടതു [Sky safari](https://play.google.com/store/apps/details?id=com.simulationcurriculum.skysafari5&hl=en_US) ആപ്പ് ഇൻസ്റ്റാൾ ചെയ്യുക എന്നതാണ്.  

### ഇനി ആപ്പ് എങ്ങനെ ഉപയോഗിക്കാം എന്നു നോക്കാം. 
 1. ആപ്പ് തുറക്കുക. അപ്പോൾ അവർ ആവശ്യപ്പെടുന്ന Media & Location - permissions അനുവദിച്ചു നൽകുക.
 
 2. ഇനി താഴെ കാണുന്ന ചിത്രത്തിലെ പോലെ **search** ബട്ടൺ ക്ലിക്ക് ചെയ്യുക .
 
![search](https://user-images.githubusercontent.com/53209422/98445405-a16e1580-213d-11eb-8a52-0aa481766f73.jpg)
 
 3. അപ്പോൾ വരുന്ന മെനുവിൽ നിന്നും **Brightest comets** ക്ലിക്ക് ചെയ്യുക .
 

![brighter](https://user-images.githubusercontent.com/53209422/98445396-9d41f800-213d-11eb-8f83-b1b27ec84dfa.jpg)
 
 4. അടുത്തതായി വരുന്ന സ്‍‍ക്രീനിൽ ആദ്യം വരുന്ന **C/2020 F3 (Neowise)** ക്ലിക്ക് ചെയ്യുക.
 
 ![neowise](https://user-images.githubusercontent.com/53209422/98445391-9a470780-213d-11eb-9b1f-dcbbc37357ae.jpg)
 
 5. അടുത്ത സ്‍‍ക്രീനിൽ comet Neowise -ന്റെ വിവരണം ലഭ്യമാകും. ഇനി സ്‍‍ക്രീനിന്റെ താഴെ  ഇടതു  മൂലയിൽ കാണുന്ന **Center** എന്ന icon-ൽ ക്ലിക്ക് ചെയ്യുക.
 
![focus](https://user-images.githubusercontent.com/53209422/98445389-97e4ad80-213d-11eb-889a-1496e82b57f2.jpg)
 
 6. ഇത്രയും ചെയ്തു കഴിയുമ്പോൾ comet എവിടെ ആണ് ഇപ്പോൾ എന്ന് സൂചിപ്പിക്കുന്ന ഒരു സ്‍‍ക്രീൻ വന്നിട്ടുണ്ടാകും. ഇനി ചെയ്യേണ്ടത് നിങ്ങളുടെ മൊബൈൽ ഫോൺ സ്‍‍ക്രീനിൽ കാണുന്ന ദിശ സൂചകം അനുസരിച്ച് തിരിക്കുക എന്നത് മാത്രമാണ് . വൈകുന്നേരം വടക്കു പടിഞ്ഞാറു ദിശയിൽ ആശാൻ ഇങ്ങനെ നിൽപുണ്ടാകും.
 
 ![target](https://user-images.githubusercontent.com/53209422/98445386-95825380-213d-11eb-8fac-36df6375f3c5.jpg)
 
 
## Bonus
ഇനി നിങ്ങൾക്ക് ആവശ്യമായത് വടക്കു പടിഞ്ഞാറു ദിശ വ്യക്തമായി കാണുന്ന സ്ഥലം കണ്ടെത്തി അവിടേക്ക് പോകുക എന്നത് മാത്രമാണ്. Time travel ഓപ്ഷനും ആപ്പ് നൽകുന്നുണ്ട് .


![time_travel](https://user-images.githubusercontent.com/53209422/98445402-9fa45200-213d-11eb-88bd-71e88f708ff7.jpg)

 
**Science class** എന്ന youtube ചാനെലിൽ വന്ന  വിശദമായ  ഒരു വിഡിയോ കൂടി കണ്ട് നോക്കു.
{{< youtube gGXARIqqpjI >}}
 
Enjoy.
 

[^1]:https://en.wikipedia.org/wiki/C/2020_F3_(NEOWISE)

