---
title: "Convert a shapefile into a interactive online map"
date: 2021-05-07T17:05:59+05:30
description: "A tutorial on how to convert a shapefile into publishable online map using QGIS, Github and gist"
draft: false
author: Arun Gopinath
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
tocLevels: ["h2", "h3", "h4"]
tags:
- GIS
series:
-
categories:
- tutorials

---




# Introduction 

Imagine you have a shapefile (.shp) with you. Now you want to show the interactive map to your colleague. There are two options:
* Share your shapefile via e-mail 


OR



* Share as a link (more user friendly).
 Not everytime you are equipped with ArcGIS or QGIS. Its difficult to open those files in your mobile devices. 
 So a easy link which opens an interactive map will be cool idea to checkout.

# Let's begin

 From this point, I assume you have a shapefile with you. Incase you don't have use this file from [previous post]({{< ref "/post/tutorials/assembly2021.md" >}}).

> Download [shapefile](https://github.com/arungop/kerala-LA-election-2021-main/archive/refs/heads/master.zip) 

Now this is our plan.

* Convert shapefile into Geojson format (Using QGIS or ArcGIS or any other platform)
* Start a github account ( [Learn more](https://en.wikipedia.org/wiki/GitHub) )
* Learn how to create a gist ( A code snippet sharing platform)
* Sharing the created gist as an interactive map

## Steps

1. Open the shapefile in **QGIS**.
2. Right click the shapefile in the layers tab. Then, Export --> Save Features as.
![Export option ](/jpg/map/export.webp)
3. Select format as **GeoJSON**, Choose **file destination** and select **Reference system(CRS)**. Then click **Ok**.
![Export to GeoJSON format](/jpg/map/ke.webp)
 * Enable *Add saved file to map* to view the successfully converted file. Now we have a file ready to upload.
4. Go to github.com and [create an account](https://github.com/join). 
5. After successfully creating an account in Github, go to [gist.github.com](https://gist.github.com).
![Open gisy](/jpg/map/gist.webp)
6. Easiest way to add the file is drag and drop method. That means open the folder containing the *.geojson* file.
Drag the file and drop in the blank space (check figure).

![Drag and drop](/jpg/map/drag.webp)
> If the blank space is filled with text data, then proceed to next step.
7. Most important part - make sure that you name the file you just copied with *.geojson* extension.
![Name the file with .geojson extension](/jpg/map/naming.webp)
8. Select **Create public gist** ( Secret gist cannot be used because our aim is to share the map with public ) . Next page will load our interactive map.
9. Select **Share** option and **copy the link**.
![Share options](/jpg/map/share.webp)
![Share link](/jpg/map/link.webp)

* Instead of sharing as link you can embed the map in your website (if you have one ).

10. What are you waiting for ? Happy sharing. 

> Output of this tutorial can be [view here](https://gist.github.com/arungop/8b6fd47102ba44080a33ebae32172010).


