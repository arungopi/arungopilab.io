---
title: Setup automatic scale bar of SEM / FESEM images using metadata in ImageJ
author: Arun Gopinath
date: '2023-08-08'
draft: False
slug: []
categories:
  - microscopy
  - ImageJ
  - tutorials
tags:
  - plugin
  - SEM
  - FESEM
  - zeiss
  - metadata
---


# Setup automatic scale bar of SEM / FESEM images using metadata in ImageJ

Manual setting of  *scale bar* in a SEM or FESEM images accurately is a task itself. If you spend a 5 mins to set your **ImageJ** software, then setting scale bar will be an easy task forever. Note: all the steps upto installation of **TM tools** are only needed to do once. 

## Installation of EM tools for ImageJ
1. Open **ImageJ (Fiji)** application. 	
![](images/1.png)


2. As you notice, **EM tool** will be absent in your tool bar. Follow the below steps to obtain it.
Go to **Help → Update**. Then click **update**.


![](images/2.png)

3. A pop-up window will appear with an updated progress bar. After it finishes the basic checkups, a window will appear. It may or may not show a list of sources based on your preferences. 

![](images/3.png)

4. Click on **Manage update sites**

       
5. Select **EM Tool → Apply** and **close**
![](images/4.png)

6. It will show a list of files to install. Proceed to install and after finishing update, *restart imageJ software to implement our EM tools*.


7. If everything was successfully completed, then you should see EM tools like in [figure 1](images/1.png). To implement your SEM/FESEM image, follow the next three steps. 



## Steps for regular usage
1. Open SEM/FESEM image obtained from Zeiss electron microscope (.tif file )

![](images/5.png)

2. When your image opens, select **EM Tools → SEM Zeiss metadata scale.**

![](images/6.png)


3. A log window with metadata of the SEM images will appear. It has all the necessary data related to zeiss microscope (model number etc..) and image generated. 

> **Keep the window open as such**.

![](images/7.png)

4. Now select **Analyze → Tools → Scale Bar..**

![](images/8.png)


5. Adjust parameters to get scale placed properly. Some editing options are also available.

![](images/9.png)

> **Save the image for further analysis.**

### Source
> [Github repo](https://github.com/IMBalENce/EM-tool)

All credits goes to creater of this plugin/macros. Since I'm currently working with Zeiss microscope tutorial uses zeiss options. Please make sure that you are choosing right maker name in **EM Tool**. 
