---
title: Adding a leaflet map with a popup
description: Thanks to Simon Frey 
date: 2020-03-08
tags: ["leaflet" ]
categories:
- tutorials
 
   
---

# Map with a popup!
 
 

Below map is embedded in this post with the help of shortcode created by [Simonfrey](https://github.com/simonfrey/hugo-leaflet) which uses [OSM](https://www.openstreetmap.org/) and [Leaflet](https://leafletjs.com/). A popup is added like a cherry on the top of a cake. Placing *layout/shortcode* in */themes* folder was an issue in the beginning. Later this folder is moved into the root folder of this blog. This change practically resolved the issue with  adaptability of **leaflet** shortcode. Hope this transition will be beneficial in future theme migration. 


 
{{< load-leaflet >}}

{{< leaflet-simple mapHeight="400px" mapWidth="100%" mapLat="8.423385" mapLon="77.12120" markerLat="8.423385" markerLon="77.12120" markerContent="I'm here" >}}



**Click on the marker to see the popup**
  



```

{ {< load-leaflet >} }

{ {< leaflet-simple mapHeight="500px" mapWidth="100%" mapLat="8.423385" mapLon="77.12120" markerLat="8.423385" markerLon="77.12120" markerContent="I'm here" >} }


```

