---
title: Visualising Election Ink in 3D
author: Arun Gopinath
date: '2020-12-08'
description: Election Ink aka Silver nitrate solution in 3D using Molview
show_toc: true
categories:
- tutorials
tags:
- election
- chemistry

---

# Election day thought

I was at the polling station to take part in covid19 election era. As usual, the violet ink marked my participation in the mega event. This ink is a super solution capable of changing the lives of the people. After reaching home, all I want to know is " what is this solution? ". The answer is Silver nitrate. According to Wikipedia,  the exact composition consists of alcohol and biocides for longevity and to ensure bacteria not transferred from voter to voter.

## Election Ink

> Electoral ink, indelible ink, electoral stain or phosphoric ink is a semi-permanent ink or dye that is applied to the forefinger (usually) of voters during elections in order to prevent electoral fraud such as double voting. It is an effective method for countries where identification documents for citizens are not always standardised or institutionalised. Election ink uses silver nitrate, and excessive exposure can cause argyria. It was first used during the 1962 Indian general election, in Mysore State, now the modern-day state of Karnataka.[^1](https://en.wikipedia.org/wiki/Election_ink)

Silver nitrate is a compound known even for the elementary school students, but how it looks in 3D? The search ends in [Molview](https://molview.org/). All you want to do is search for Silver nitrate or silver nitrate solution. There are numerous options to play around. Van der Waal spheres are 3D representation option with axis rotation. The output is available in ['.mol' format](https://drive.google.com/file/d/1IUZko9_Ez-NqnZkJczW2sf7xFahGkDlg/view?usp=sharing).

Chemical formula: AgNO<sub>3</sub>

### 3D view

Interactive Silver Nitrate compound in Molview.

<iframe style="width: 500px; height: 300px;" frameborder="0" src="https://embed.molview.org/v1/?mode=vdw&cid=24470"></iframe>

 

That white ball represents Silver and three red balls are Oxygen molecules. The jammed blue ball is non-other than Nitrogen. 

### Other options

More interactive and animated molecules can be view in [virtual Chemistry 3D](http://vchem3d.univ-tlse3.fr/) created by [Romuald Poteau](http://romuald-poteau.blogspot.com/).One can import already created '.mol' files for advanced animations and analysis.



That white ball represents Silver and three red balls are Oxygen molecules. The jammed blue ball is non-other than Nitrogen.

### Other options

More interactive and animated molecules can be view in [virtual Chemistry 3D](http://vchem3d.univ-tlse3.fr/) created by [Romuald Poteau](http://romuald-poteau.blogspot.com/).One can import already created '.mol' files for advanced animations and analysis.
