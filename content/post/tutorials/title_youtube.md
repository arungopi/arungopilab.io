---
title: Automatically update the titles in your Youtube videos.
author: Arun Gopinath
date: '2020-11-17'
description: Implement the changing title in Youtube videos using Google script.
show_toc: true
categories:
  - tutorials
tags:
  - youtube
---




# Magic video

The idea of changing the title of the youtube video automatically seems insane to me. There is a tutorial about how to achieve this feat is thoroughly done by [Amit Agarwaal](https://digitalinspiration.com/).  The original concept was by [Tom Scott]( youtu.be/BxV14h0kFs0). This article is all about the implementation of the same in my video.

Youtube api and Google scripts are primarily responsible for this weird feat. Most of the contents of this article are from [labnol.org](labnol.org). 

##  What you have to do ?

* Copy this [google script](https://www.labnol.org/topic/google-apps-script) to your google drive.

* Now, go to your youtube video and look for the _youtube id_. It's more like the tail end of the link. For my case, video link is [https://youtube.com/watch?v=z_-P9oLM92k](https://youtube.com/watch?v=z_-P9oLM92k) and z_-P9oLM92k is the id.

* So you have your video id. Copy this code and paste the same in the script. 

```
const id = "z_-P9oLM92k";
```
* The video title is basically the next title.

```
const template = 'This timelapse of fisherman has VIEWCOUNT views and counting';

```
* The **VIEWCOUNT** and **COMMENTCOUNT** are the two variables. Modify the title and place the variables in cool positions. I have omitted the commentcount.

* This script set to run in every minute.

```
// If time based trigger doesn't exist, create one that runs every 1 minutes
    if (triggers.length === 0) {
      ScriptApp.newTrigger(triggerName).timeBased().everyMinutes(1).create();
    }
  }


```
* The coding part is done. Now select **Run** --> **Run function** --> **UpdateYoutubeVideo**.

![Run function](https://user-images.githubusercontent.com/53209422/99362924-1e398600-28da-11eb-8840-187bc53a9e24.png)

* Give permissions and Enjoy.

{{< youtube z_-P9oLM92k>}}


