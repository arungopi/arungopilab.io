

---
title: "Exploring Space in Real-Time: A Shiny App Adventure"
author: Arun Gopinath
date: '2023-12-16'
draft: False
slug: []
categories:
  - tutorials
  - Data science
tags:
  - Dashboard
  - RShiny
  - ISS
  - hobby
---


# Exploring Space in Real-Time: A Shiny App Adventure

![Space Exploration](images/scr_1.jpg)

![Space Exploration](images/scr_2.jpg)

## Introduction

Space has always fascinated humanity, and with the advent of technology, we now have the ability to track the movement of astronauts and spacecraft in real-time. In this blog post, we'll explore a Shiny app that provides a captivating experience of monitoring the current inhabitants of space and the live position of the International Space Station (ISS).

## The Shiny App

### Libraries and Functionality

The Shiny app is built using R and leverages several key libraries:

- **shiny:** For creating interactive web applications.
- **httr:** For making HTTP requests.
- **jsonlite:** For working with JSON data.
- **leaflet:** For interactive maps.
- **shinydashboard:** For creating dashboards.
- **shinythemes:** For theming the app.
- **htmlwidgets:** For embedding HTML widgets.

The app includes a function `getWikipediaLink` to fetch Wikipedia links for astronauts based on their names. This feature allows users to dive deeper into the background of each astronaut.

```
# Function to get Wikipedia link for a given person
getWikipediaLink <- function(name) {
  tryCatch({
    # Search for the Wikipedia page using the person's name
    wikipedia_url <- paste0("https://en.wikipedia.org/wiki/", gsub(" ", "_", name))
    # Validate if the Wikipedia page exists
    if (httr::GET(wikipedia_url)$status_code == 200) {
      return(wikipedia_url)
    } else {
      return(NULL)
    }
  }, error = function(e) {
    return(NULL)
  })
}
```

### Real-Time Data Retrieval

The app fetches real-time data from two APIs:

1. **People in Space API:** Retrieves information about the astronauts currently in space.
2. **ISS Location API:** Provides the live location of the International Space Station.

The app then converts the latitude and longitude data into numeric values for mapping purposes.

```
# People in space right now
astro <- httr::GET("http://api.open-notify.org/astros.json")
data <- jsonlite::fromJSON(rawToChar(astro$content))

# Live ISS location
live <- httr::GET("http://api.open-notify.org/iss-now.json")
now <- jsonlite::fromJSON(rawToChar(live$content))

# Convert latitude and longitude to numeric
now$iss_position$latitude <- as.numeric(now$iss_position$latitude)
now$iss_position$longitude <- as.numeric(now$iss_position$longitude)
```

## User Interface

### Dashboard Overview

The user interface is designed with a sleek and modern look using the "flatly" theme. The dashboard consists of three main sections:

1. **Total People in Space:** A visually appealing box displaying the total number of people currently in space, adorned with an icon of people to provide a quick visual cue.

```
# Display total number of people in space in a square box
valueBox(
  value = length(data$people$name),
  subtitle = "Total People in Space",
  icon = icon("users"),
  color = "blue"  # Change 'primary' to 'blue'
)
```

2. **List of People in Space:** A table showcasing the astronauts along with hyperlinks to their Wikipedia pages. This section allows users to explore detailed information about each astronaut.

```
# Create a new data frame with hyperlinked names
hyperlinked_data <- data.frame(
  name = sapply(seq_along(data$people$name), function(i) {
    if (!is.null(wikipedia_links[i])) {
      sprintf('<a href="%s" target="_blank">%s</a>', wikipedia_links[i], data$people$name[i])
    } else {
      data$people$name[i]
    }
  }),
  stringsAsFactors = FALSE
)

# Display the table with hyperlinked names
dataTableOutput('iss')
```

3. **Live Position of ISS:** An interactive map displaying the real-time location of the ISS. Users can zoom in and out and click on the marker to view detailed information about the ISS's latitude and longitude.

```
# Render ISS live location on a leaflet map
output$map <- renderLeaflet({
  leaflet() %>%
    addTiles() %>%
    setView(lng = now$iss_position$longitude, lat = now$iss_position$latitude, zoom = 8) %>%
    addMarkers(
      lng = now$iss_position$longitude,
      lat = now$iss_position$latitude,
      popup = sprintf("Latitude: %s<br>Longitude: %s", now$iss_position$latitude, now$iss_position$longitude)
    )
})
```

### Additional Tabs

The section includes **about app** and **about me** sections.

## Conclusion

In a world where space exploration is becoming more accessible, this Shiny app provides a unique and engaging way to stay updated on the whereabouts of astronauts and the ISS. Whether you're a space enthusiast, a student, or just curious about what's happening above our planet, this app offers a visually appealing and informative experience.

🌐 **Explore the Shiny app** [here](https://arun-gopinath.shinyapps.io/People_space_now/).

👨‍💻 **GitHub Repository:** [People in Space Explorer on GitHub](https://github.com/arungop/people_in_the_space_right_now)

For more information about the developer, visit Arun Gopinathan's [LinkedIn profile](https://www.linkedin.com/in/arungopinat/).

Happy exploring the cosmos in real-time! 🚀✨

