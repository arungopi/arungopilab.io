---
title: "Creating a simple corona virus model using blender"
date: 2020-04-18T18:06:05+05:30
description: "Creating a 3D Covid19 model"
author: Arun Gopinath
draft: false
tags: ["covid 19","blender"]
categories:
- tutorials

---

# Blender-ൽ ഒരു കൊറോണ വൈറസ് 

ചുമ്മാ internet-ൽ തപ്പി നേരം പോയിരിക്കുമ്പോൾ ആണ് ഒരു ചിന്ത പെട്ടെന്ന് വന്നത് _([ചിന്താ ജെറോം](https://www.youtube.com/watch?v=ChdKBh2u12Q) അല്ല ഇതു വേറെ)_. **[Blender](https://www.blender.org/)** എന്നൊരു _software_ ഉണ്ട്  നിങ്ങളിൽ പലർക്കും അറിയാവുന്നതായിരിക്കും എന്നാലും ഈ blender, **_3D  animations_** ഒക്കെ ഉണ്ടാക്കുവാനുള്ള ഒരു അടിപൊളി **Free & open source software(FOSS)** ആണ്. സത്യം പറഞ്ഞാൽ അതിന്റെ _ABCD_ എനിക്ക് അറിയില്ലായിരുന്നു ഈ ചിന്ത ഉണ്ടാകും വരെ. ഒരു കുഞ്ഞൻ കൊറോണയേ blender ൽ ഉണ്ടാക്കിയാൽ ആത് ഒരു രസം തന്നെ. പക്ഷേ youtube -ൽ നോക്കിയപ്പോൾ item അവിടുണ്ട്. ഞാനായിട്ട് ഒരു പുതുമയും ഇവിടെ കൊടുക്കുന്നില്ല പകരം ഞാൻ നിർമിച്ച രസികൻ കൊറോണയെ ഇവിടെ പങ്കുവെക്കാം. 
  
> ഇതൊരു നേരം പോക്കായി കാണുക Covid19 virus ഇങ്ങനെ ഈ രൂപത്തിൽ ആണെന്ന് ഇതിനൊരു അർഥവും ഇല്ല.


## 3D Corona 

{{< youtube bU1N5kiJWp0>}}


# Wallpaper



  
  Wallpaper ആക്കാനും അടിപൊളി ആണ്. Sample ചുവടെ.
  
  
![Screenshot](https://user-images.githubusercontent.com/53209422/98446979-8b655280-2147-11eb-8396-a7261e0a503d.jpg)
 
  

## Download section

  [Download ചെയ്യാൻ ഇവിടെ ക്ലിക്ക് ചെയ്യുക](/posts/Blender/Wallpaper.jpg).

 

## Blender files

Blender source files which can be used to replicate the model are uploaded to github. Feel free to use.

 [Source files](https://github.com/arungop/Covid19-blender)




