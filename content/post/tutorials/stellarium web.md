---
title: "ആപ്പ് ഇൻസ്റ്റാൾ ചെയ്യാതെയും കാണാം Comet Neowise "
date: 2020-07-17T15:00:04+05:30
description: "Stellarium web app ഒരു ബദൽ മാർഗം "
draft: false
show_toc: true
tocLevels: ["h1","h2", "h3", "h4"]
tags: ["astronomy", ]
series:
-
categories:
- tutorials
image: images/feature2/astro.png
---

Sky safari ഉപയോഗിച്ച് comet neowise എങ്ങനെ കാണാം എന്നു മുൻപത്തെ ബ്ലോഗ്ഗിൽ നാം കണ്ടതാണല്ലോ. ചിലർക്കെങ്കിലും ആപ്പ് ഒക്കെ ഇൻസ്റ്റാൾ ചെയ്ത് കൊമെറ്റിനെ കാണാൻ മടിയായിരിക്കും. അങ്ങനെ ഉള്ള അന്വേഷണം എത്തി ചേർന്നത് open source planetarium ഭീമനായ Stellarium -ന്റെ website -ൽ ആണ്. അവരുടെ തന്നെ web ആപ്പ് ആണ് നമ്മുടെ സമസ്യക്കുള്ള ഉത്തരം.


# Stellarium website

**[https://stellarium-web.org/](https://stellarium-web.org/)**


> ഈ website എല്ലാത്തരം operating system -ലും ഒരു പോലെ ഉപയോഗിക്കാവുന്നതാണ്.അതുകൊണ്ട് തന്നെ മൊബൈലിൽ ഈസിയായി എപ്പോൾ വേണമെങ്കിലും ഇന്റെർനെറ്റിന്റെ സഹായത്തോടെ ഉപയോഗിക്കാം.


   


## എങ്ങനെ ഉപയോഗിക്കാം ?
1. ആദ്യം തന്നെ [https://stellarium-web.org/](https://stellarium-web.org/) സന്ദർശിക്കുക.

![homepage](https://user-images.githubusercontent.com/53209422/98445499-0de91480-213e-11eb-9c81-e614cc45d19e.png)


2. നിങ്ങൾ മൊബൈലിൽ ആണ് എടുക്കുന്നതെങ്കിൽ location access അവർ ചോദിക്കും.നിങ്ങൾക്കു അപ്പോൾ തന്നെ access കൊടുക്കാവുന്നതാണു. (ഡെസ്ക്‍ടോപ്പിൽ location access കൊടുത്താലും gps ഇല്ലാത്തതിനാൽ accuracy കാണില്ല.)
 

 
3. ഇനി നിങ്ങൾ privacy ആണ് മുൻഗണന നൽക്കുന്നതെങ്കിൽ location manual ആയി എന്റെർ ചെയ്യാനും വഴി ഇതിലുണ്ട്.അതിനായി താഴെ ഇടതു മൂലയിൽ കാണുന്ന  ഈ option ക്ലിക്ക് ചെയ്യുക.
 
![location_tool](https://user-images.githubusercontent.com/53209422/98445494-09bcf700-213e-11eb-9aad-ba3b7397f1fe.png)
 

4. അപ്പോൾ തെളിഞ്ഞു വരുന്ന window -ൽ location സെ‍ർച് ചെയ്യാൻ അവസരം ഉണ്ട് അങ്ങനെ സെ‍ർച് ചെയ്തു എന്റെർ ചെയ്യുമ്പോൾ വരുന്ന ലിസ്റ്റിൽ നിന്നും നിങ്ങളുടെ ഇഷ്ടാനുസരണം സെലെക്ട്ട് ചെയ്യാം. അതിന് ശേഷം Use this location സെലെക്ട്ട് ചെയ്യുക.

![location_fix](https://user-images.githubusercontent.com/53209422/98445488-088bca00-213e-11eb-966b-4f42944db45f.png)

 
5. ഇനി ചെയ്യേണ്ടത് സമയം മാറ്റുക എന്നതാണ്. നിങ്ങൾക്കു ആവശ്യമുള്ള സമയം താഴെ വലതു മൂലയിൽ സെറ്റ് ചെയ്യാൻ അവസരം ഉണ്ട്. ഇഷ്ടമുള്ള ദിവസം ഇഷ്ടമുള്ള സമയം സെലെക്ട്ട് ചെയ്യാം. അതിൽ തന്നെ രാത്രിയും പകലും വേഗത്തിൽ മാറ്റാൻ ഒരു വഴിയും കൊടുത്തിട്ടുണ്ട് , അതിലൂടെ ഒന്നോടിച്ചു പോയാൽ അതും വേഗം മനസിലാകും.

![time](https://user-images.githubusercontent.com/53209422/98445486-06c20680-213e-11eb-975a-c3ebe4b6c3d9.png)

6. ഇപ്പോൾ realtime ആയി തിരിച്ചു വരാനും ഇതിൽ അവസരമുണ്ട്.


7. സമയം വൈകിട്ട് സെറ്റ് ആക്കിയാൽ നമുക്ക് comet Neowise തെളിഞ്ഞു കാണാം. സമയം ചെറുതായി മാറ്റി നോക്കി എവിടെ എത്ര ഉയരത്തിൽ എത്തുമെന്നു അറിയാൻ കഴിയും.  കൂടുതൽ സഹായത്തിന് താഴെ കുറെ ഏറെ options ഉണ്ട്. ആത്  Constellations, atmosphere എന്നിങ്ങനെ പോകുന്നു. പകൽ-  രാത്രി ഒക്കെ സെറ്റ് ചെയ്തു നോക്കാനും കഴിയും.

![Day](https://user-images.githubusercontent.com/53209422/98445483-04f84300-213e-11eb-93df-a0edd258edbf.png)
 
8. Constellations കൂടി ഉപയോഗപ്പെടുത്തി വേഗം Neowise നെ കണ്ടെത്തുകയും ആവാം.


![const](https://user-images.githubusercontent.com/53209422/98445473-f742bd80-213d-11eb-89bd-e4e1cc38a948.png)
   

9. Atmosphere എന്ന option ഇട്ടു കഴിഞ്ഞാൽ ശരിക്കമുള്ള ആകാശം അറിയാൻ കഴിയും. താഴെ ഉള്ള ചിത്രങ്ങൾ നോക്കുക.


 
  
 Atmosphere option enabled screen.
 
![with_atm](https://user-images.githubusercontent.com/53209422/98445478-0164bc00-213e-11eb-803f-008d3bf1c906.png)
   

 
 
 Atmosphere option disabled screen.
 

![without_atm](https://user-images.githubusercontent.com/53209422/98445476-fe69cb80-213d-11eb-8238-2a13274394f6.png)
 




10. കൂടാതെ ഏതൊരു planaterium software നൽകുന്ന സാങ്കേതിക സഹായവും stellarium വെബ്സൈറ്റിൽ ലഭിക്കുന്നതാണ് .
