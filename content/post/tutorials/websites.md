---
title: Curated list of amazing websites - Part 1
author: Arun Gopinath
date: '2020-11-23'
show_toc: true

description: A curated list of awesome websites
categories:
  - tutorials
tags: 
  - amazing websites
---




# Amazing websites

There are websites which are fun to surf in the web world. Some of them are useful in day-to-day life. But there are some websites which are made with the sheer genius and make us wonder “How they made this?”. So, this is a curated list of such websites Part 1.

## WebGL fluid simulation


 As the name suggests, one can play with fluid simulation all day. It works on any web browser irrespective of their OS platform. There is a mobile app version for this website. There are a lot of control options in the sidebar to work around. This is a dedicated work by [Paveldogreat](https://github.com/PavelDoGreat).

![web_fluid](https://user-images.githubusercontent.com/53209422/100114256-3dff1a00-2e97-11eb-919c-144b3a18d3be.jpg)

[Visit]((https://paveldogreat.github.io/WebGL-Fluid-Simulation/))

Check the source code:  [<img src="https://s18955.pcdn.co/wp-content/uploads/2018/02/github.png" width="25"/>](https://github.com/PavelDoGreat/WebGL-Fluid-Simulation)

## Muscle wiki

You don't have to be a fitness freak to understand this site. Simply select a muscle it provides you with the exact exercises to workout that muscle (either video or gif format). They are keen to include both the male and female versions. There are more cool features hidden to checkout.

* If you select the biceps:

![Muscle enquiry](https://user-images.githubusercontent.com/53209422/100117107-7c4a0880-2e9a-11eb-8a8c-244c9a9deef8.jpg)

![Muscle result](https://user-images.githubusercontent.com/53209422/100117103-7b18db80-2e9a-11eb-9c70-f4507b229d7e.jpg)


[Visit](https://musclewiki.com/)

## Radio Garden

One of the popular visual treat on the internet. Radio Garden is a non-profit Dutch radio and digital research project developed from 2013 to 2016, by the Netherlands Institute for Sound and Vision (under the supervision of Martin Luther University of Halle-Wittenberg's Goal Föllmer), by the Transnational Radio Knowledge Platform and five other European universities.[^1]



You have a globe in front of you with numerous radio stations broadcasting live. Just rotate, search, zoom or even pinch to find out the radio stations according to your mood. Sound quality and buffering are quite good. They also have a dedicated mobile application.

![Radio garden](https://user-images.githubusercontent.com/53209422/100118924-53c30e00-2e9c-11eb-8da0-0a7d063ace4b.jpg)


[Visit](http://radio.garden/)


## What every Browser knows about you

Your browser knows almost everything about you. Here is the proof. Check out yourself. This website is a creation of [Robin Linus](https://github.com/robinlinus).


[Visit](https://webkay.robinlinus.com/)


## Outrider.org

![nuclear_impact](https://user-images.githubusercontent.com/53209422/100122467-6ab72f80-2e9f-11eb-859c-c0a73fec537f.jpg)

Did you ever think about the devasting power of Nuclear bombs? What if the bomb just dropped over your location? Here you can see the approximate impact of the nuclear bombs on any location on the earth. One can change the bomb and check various impact parameters.


[Visit](https://outrider.org/nuclear-weapons/interactive/bomb-blast)


## What it's like to have dyslexia

Dyslexia, also known as reading disorder, is characterized by trouble with reading despite normal intelligence [^2]. This site provides the exact trouble to normal readers by randomizing the letters each second. Created by [Victor Widell](http://geon.github.io/) 


![Dyslexia](https://user-images.githubusercontent.com/53209422/100124297-7c99d200-2ea1-11eb-8e76-038ffd940393.jpg)


[Visit](http://geon.github.io/programming/2016/03/03/dsxyliea)

## lines.chromeexperiments.com

![Lines chrome](https://user-images.githubusercontent.com/53209422/100127627-4f4f2300-2ea5-11eb-9beb-d697ed428c0d.jpg)

Simply draw a line or curve, the result will surely amaze you. Corresponding geometry on the earth surface will show immediately. This is a pet site of google with the blessings of ever growing image library of Google Earth.

[Visit](https://lines.chromeexperiments.com/)

## Deskspacing


If you are planning to sort out your desk which is a total mess right now, then deskspacing is the right tool for you. Play with different objects, camera angles and finally arrive at your dream arrangement. Sorry mobile users this gem of a site is currently has Desktop only experience.

![Deskspacing](https://user-images.githubusercontent.com/53209422/100129110-1ca62a00-2ea7-11eb-8630-f8ce88f62e4b.jpg)


[Visit](https://deskspacing.com/)

## Wikiverse



Wikipedia is now becoming the go-to knowledge base for most people. Imagine the quantity of the articles there. Wikiverse is such a platform where you can visualise the Wikipedia as a universe full of galaxies and stars (wiki articles). The size of the globe and the number of articles to display can change. The interactive interface is fun to explore. Created by [Owen Cornec](http://byowen.com/) and **not** affiliated to Wikipedia or Wikimedia.

![Wikiverse](https://user-images.githubusercontent.com/53209422/100130375-a5719580-2ea8-11eb-8d7c-47c45b5373fc.jpg)


[Visit](http://wikiverse.io/)

## Emupedia

If you still missing the old windows version. Even the apps are working fine. A classic tribute to  a vintage operating system. 


![Old windows](https://user-images.githubusercontent.com/53209422/100131825-8d9b1100-2eaa-11eb-854e-47f6840f2005.jpg)


[Visit](https://emupedia.net/beta/emuos/)

## Windy.com

An all-rounder website to weather-related queries. **Windy.com** provides far better visualization of weather phenomenon. Their forecasting models with a wide variety of data toggles make this unique website to checkout.

[Visit](windy.com)

### Cyclone tracker

A live cyclone tracker by Windy.com.

<iframe width="400" height="600" src="https://embed.windy.com/embed2.html?lat=7.450&lon=78.552&detailLat=8.429&detailLon=76.959&width=400&height=600&zoom=7&level=surface&overlay=gustAccu&product=ecmwf&menu=&message=true&marker=&calendar=now&pressure=true&type=map&location=coordinates&detail=true&metricWind=km%2Fh&metricTemp=%C2%B0C&radarRange=-1" frameborder="0"></iframe>

Liked this blog post? Con­sid­er [buy­ing me a cof­fee!](https://www.buymeacoffee.com/arungopi)


[^1]:https://en.wikipedia.org/wiki/Radio_Garden

[^2]:https://en.wikipedia.org/wiki/Dyslexia
