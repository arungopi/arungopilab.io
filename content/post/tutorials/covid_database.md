---
title: "Covid19 kerala database"
author: "Arun Gopinath"
date: 2020-04-02T22:17:29+05:30
description: "Important links and official covid19 database of kerala"
tags: ["covid 19"]
categories: ["tutorials"]
---




# Live Covid19 patient database of kerala

[Google sheet database with regular updates](https://docs.google.com/spreadsheets/d/e/2PACX-1vQU9eLCMT0XwWnoxV_LkyCkxMcPYO7z7ULdODoUFgcdzp48pgGpGrVZFXvraXYvUioVRsQgQDU_pQyI/pubhtml)



**SOURCE: [https://covid19kerala.info/](https://covid19kerala.info/)**


> For detailed infographics visit above website. This is an open source project which is cloned from Covid19japan [^1] and Maintained by Volunteers at CODD-K Team. Supported by Government College Kasaragod. Also available in both malayalam and english languages. 

## Rshiny apps	

<iframe height="800" width="100%" frameborder="no" src="https://arun-gopinath.shinyapps.io/project/"> </iframe>

[^1]: [Covid19japan](https://covid19japan.com/)
