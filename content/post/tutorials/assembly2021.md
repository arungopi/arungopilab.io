---
title: 'Detailed map of Kerala assembly elections 2021'
author: Arun Gopinath
date: '2021-05-05'
description: A detailed map of kerala assembly elections 2021. Output is hosted on github. Available formats are geojson, esri shape file and csv. Free feel to share
show_toc: true
draft: false
categories:
  - tutorials
tags: 
  - assembly election
  - infographics
---
# Kerala assembly election results 2021

The 2021 Kerala Legislative Assembly election was held in Kerala on 6 April 2021 to elect 140 members to the 15th Kerala Legislative Assembly. The results declared on 2 May.

## Results in a nut shell

| Party | Seats |
|-|-|
| LDF | 99 |
| UDF | 41 |
| NDA | Nil |
| Total seats | 140 |


This article provides the shapefile containing the details of candidates in each legislative assembly. Shapefile used here are obtained from Kerala Open Data. Details such as Party, Second best candidate & their party and winning margin are also incorporated. 

Source code: [Github](https://github.com/arungop/kerala-LA-election-2021-main)

> This is an updated version of map created by [Kerala open data](https://github.com/opendatakerala/kerala-assembly-map)

## Detailed map

{{< gist arungop bf5964fd121b7b018d131da7d4de259a  >}}

<a href="https://gpay.app.goo.gl/pay-sCjOGpyBhXM" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A " style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>
