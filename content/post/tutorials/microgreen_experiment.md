---
title: "How to create a microgreen recipe"
date: 2020-04-12T18:28:47+05:30
description: "Microgreen experiment"
tags: ["microgreen","health"]
categories:
- tutorials
---

മാധ്യമങ്ങളിൽ കണ്ട് പരിചയിച്ച ഒരു പേരാണ് **Microgreen**. ഇത്രക്ക് പറഞ്ഞുകേട്ട സ്ഥിതിക്ക് ഒന്നു നോക്കിക്കളയാം.

# Microgreen?

ഏകദേശം 1–3 ഇഞ്ച് (2.5–7.5 സെ.മീ) ഉയരമുള്ള ഇളം പച്ചക്കറി പച്ചിലകളാണ് മൈക്രോഗ്രീനുകൾ.ഇവ പാചകത്തിനും ശരീരത്തിനും ഉത്തമം.വെറും ഒരാഴ്ച്ച പരിശ്രമിച്ചാൽ രുചികരവും പോഷക സമൃദ്ധമായ ഒരു ഇലക്കറി ഉണ്ടാക്കി എടുക്കാം . _Microgreen_ ഉണ്ടാക്കാൻ ആവശ്യമായത് ഒരു ചെറിയ പാത്രവും കുറച്ച് വിത്തുകളുമാണു. **പയർ,ഗ്രീൻപീസ്,ഉലുവ** പോലെ വേഗം മുളക്കുന്ന വിത്തുകൾ ഉത്തമം.


# Nutritional value

> Among the 25 microgreens tested, red cabbage, cilantro, garnet amaranth, and green daikon radish had the highest concentrations of vitamin C, carotenoids, vitamin K, and vitamin E, respectively. In general, microgreens contained considerably higher levels of vitamins and carotenoids—about five times greater—than their mature plant counterparts, an indication that microgreens may be worth the trouble of delivering them fresh during their short lives[^1].

സാധാരണ ഇലക്കറിയേക്കാൾ രുചികരം എന്നാണ് വായിച്ചു അറിഞ്ഞത്. എന്നാൽ തുടങ്ങാം....

## day01

* ആദ്യം ചെയ്യേണ്ടത് നേരത്തെ പറഞ്ഞപോലുള്ള വിത്തുകൾ ഒരു പിടി എടുക്കുക. ഒരു പരീക്ഷണം എന്ന നിലക്ക് ചെറുപയർ തന്നെ നല്ലത് . ആത് വെള്ളത്തിൽ കുതിരാൻ ഒരു ആറ് മണിക്കൂർ ഇട്ട് വൈക്കുക.(തൽക്കാലം ആത് അവിടിരുന്നോട്ടേ )
* നമ്മുടെ വിത്തുകൾ പാകുവാൻ തക്ക വലുപ്പമുള്ള ഒരു പാത്രം എടുക്കുക . ഞാൻ ചെറിയൊരു കൂടയാണു എടുത്തത് . 
* ഇനി രണ്ടു വഴിയാണ് മുന്നിലുള്ളത് - മണ്ണ് ഉപയോഗിച്ച് അല്ലെങ്കിൽ പഴയ പേപ്പർ പോലുള്ളവ ഉപയോഗിച്ച്. (ഇടക്ക്  cleaning കഴിഞ്ഞത് കൊണ്ട് ധാരാളം പഴയ പേപ്പർ ഒഴിവാക്കാൻ ഒരു മാർഗവുമാണു) . ഇതിനു മൂന്നോ നാലോ താൾ പേപ്പർ മതിയാക്കും. 
* ഒരു ആറ് മണിക്കൂർ കഴിഞ്ഞ് ആ വിത്തുകൾ എടുത്ത് ഒരു ചെറിയ തുണിയിൽ ( cotton ) ഒരു പൊതി കണക്കെ ഈർപ്പത്തോടെ വൈക്കുക. **(ബാക്കി നാളെ)** . 


## day02

ഇനി simple ആണ് കാര്യങ്ങൾ. ഇന്നലെ വച്ച വിത്തുകൾ ഇപ്പോൾ മുളച്ച് കഴിഞ്ഞിരിക്കും . 

* നേരത്തേ തയ്യാറാക്കിയ ആ പാത്രത്തിൽ ഈ വിത്തുകൾ വിതറുക . 

* പണി കഴിഞ്ഞു. ഇനി ഒരു മൂന്നു നേരം അൽപ്പം വെള്ളം തളിക്കുക . ഒരുപാട് നനവായാൽ കേടാകും (ചീയ്യാൻ സാധ്യത).


## day 03 

ഇടക്കിടക്ക് നനക്കുക. കുഞ്ഞ് ഇലകൾ വരുന്നത് കണ്ട് സന്തോഷിക്കുക. 

## day04

Repeat day03

## day05

Repeat day04. More leaves means more happy.

## day06

Repeat day05. ഇപ്പോൾ ഇലകൾ [AIDMK യുടെ ചിഹ്നം കണക്കെ](https://en.wikipedia.org/wiki/All_India_Anna_Dravida_Munnetra_Kazhagam) നല്ല പച്ചപ്പായി നിൽപ്പുണ്ടാകും. അൽപ്പം കൂടി വളരട്ടെ നാളെ മുറിചെടുക്കാം.

## day07

തണ്ടും ഇലകളും ഒക്കെ മുറിച്ചെടുത്ത് നല്ല തോരൻ വച്ചു. രുചിയുണ്ട് ഗുണവും ഉണ്ട് . കുറച്ച് സന്തോഷവും .


# ഒരുകൈനോക്കം

Lockdown നീട്ടാൻ പോകുവല്ലേ ചുമ്മാ ഒരുകൈനോക്കിയെക്കാമല്ലോ. വലിയ പണിയൊന്നും ഇല്ലാന്നേ. ഇനി മണ്ണ് ഉപയോഗിച്ച് ഉള്ള വഴിയും ഉണ്ട്. 

{{< youtube YXCI4P2cNZQ >}}

[^1]: [Nutritional value](https://en.wikipedia.org/wiki/Microgreen#Nutritional_analysis)
