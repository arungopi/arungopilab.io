---
title: "Beginners guide to ERDAS IMAGINE"
date: 2020-08-06T17:05:59+05:30
description: "A basic tutorials on Erdas Imagine "
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
tocLevels: ["h2", "h3", "h4"]
tags:
- GIS
- Erdas Imagine
series:
-
categories:
- tutorials

---


During the course period of GIS in kariavattom, we colleagues created a tutorial on Erdas Imagine, a remote sensing tool. The whole project was bounded to the syllabus and are extremely useful. Later I thought it would be beneficial to the beginners if the work is available as a website. If you find this useful share it with your friends.

<!--adsense-->







[Beginners guide to ERDAS IMAGINE](https://arungopi.gitlab.io/erdas)
