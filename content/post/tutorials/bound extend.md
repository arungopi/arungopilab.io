---
title: "Extracting the boundary extends of polygons in a shapefile using QGIS and VBA in Microsoft excel"
date: 2021-01-30T17:05:59+05:30
description: "Extracting the boundary extends or bbox values of all the polygons in a shapefile using QGIS is explained here. The obtained decimal values are converted into Degree Minute Seconds form using Visual Basic for Application (VBA) in Microsoft Excel"
draft: false
author: Arun Gopinath
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
tocLevels: ["h2", "h3", "h4"]
tags:
- GIS
- QGIS
- VBA
series:
-
categories:
- tutorials

---

# Introduction

In this tutorial lets us learn how to extract the boundary limits or bbox values of each polygon in a shapefile using QGIS. First of all, imagine a shapefile consists of some number of polygons. So each polygon can be exactly fit into imaginary rectangles each having upper and lower limits of latitude and longitude values. These four limits will be the boundary for that polygon. Similarly, remaining polygons will also have such boundaries. In this article, we will extract those values into a spreadsheet format.

## Required tools

* QGIS

* Mircrosoft Excel



## Follow these steps


To calculate the boundary extend of a shapefile ( Contains multiple polygons )


1. Open the required shapefile in **QGIS**

![0](https://user-images.githubusercontent.com/53209422/106386381-b8e63300-63fa-11eb-9bdc-8def30a46145.jpg)

2. Now open attribute table

![1 ](https://user-images.githubusercontent.com/53209422/106386388-c1d70480-63fa-11eb-97e0-9139815dfe41.jpg)

3. Click Start editing button (pencil) [Ctrl + E]. 

![2 ](https://user-images.githubusercontent.com/53209422/106386397-ca2f3f80-63fa-11eb-8693-c2d0f788e2da.jpg)

Also, select 'Open Field Calculator' [ Ctrl + I ]

![3](https://user-images.githubusercontent.com/53209422/106386403-d0bdb700-63fa-11eb-9733-cb17159f522e.jpg)

4. Check create new field, Add output field name as xmin 


> xmin and xmax  represents North(upper limit) and South (lower limt) of boundary respectively. Similarly ymin and ymax represents West and East boundary limts.

5. Output field type as 'text'

6. Output field type = 20 (Optional)

7. In the Expression tab below, enter 
   
```
x_min($geometry)

```

> If everything is fine, then an output preview can be seen in the left bottom of that tab.

![4](https://user-images.githubusercontent.com/53209422/106387357-6a876300-63ff-11eb-8ef7-9529f1c383da.jpg)

8. Click OK. The attribute table will be populated with xmin values. 

![5](https://user-images.githubusercontent.com/53209422/106386413-da471f00-63fa-11eb-8b96-8516219ed845.jpg)

> The values will be in decimals. We have to convert it into DMS(Degree,Minute,Seconds) later.

*  Similarly, follow the above steps for xmax, ymin & ymax. Respective expressions will be

``` 
x_max($geometry)

y_min($geometry)
 
y_max($geometry)

```

![6](https://user-images.githubusercontent.com/53209422/106386416-df0bd300-63fa-11eb-98b1-33f8806a4b03.jpg)


9. Now in the layers panel of QGIS, right click to save as the shape file as .csv format and click save.

![7](https://user-images.githubusercontent.com/53209422/106386419-e337f080-63fa-11eb-89d3-f2da86ba6144.jpg)

![8](https://user-images.githubusercontent.com/53209422/106386424-e92dd180-63fa-11eb-86ec-ca22a81f0b51.jpg)


![9](https://user-images.githubusercontent.com/53209422/106386428-ecc15880-63fa-11eb-90af-ab1d6a698d84.jpg)


10. Now open the above saved spread sheet in Excel.

11. Add XMIN, XMAX, YMIN and YMAX columns.

![10](https://user-images.githubusercontent.com/53209422/106386431-efbc4900-63fa-11eb-8a6f-82e1aa487fbf.jpg)

12. Click Alt + F11 , which means you are opening the Visual Basic for Applications (VBA)

13. Select **Insert** , then **Module**

![11](https://user-images.githubusercontent.com/53209422/106386434-f3e86680-63fa-11eb-816e-21b0278ebc40.jpg)

14. A blank tab will open up. Copy the code given below and paste it in that blank space.

```
Function Convert_Degree(Decimal_Deg) As Variant
    With Application
        'Set degree to Integer of Argument Passed
        Degrees = Int(Decimal_Deg)
        'Set minutes to 60 times the number to the right
        'of the decimal for the variable Decimal_Deg
        Minutes = (Decimal_Deg - Degrees) * 60
        'Set seconds to 60 times the number to the right of the
        'decimal for the variable Minute
        Seconds = Format(((Minutes - Int(Minutes)) * 60), "0.000")
        'Returns the Result of degree conversion
        '(for example, 10.46 = 10~ 27  ' 36")
        Convert_Degree = " " & Degrees & "° " & Int(Minutes) & " ' " & Seconds + Chr(34)
    End With
End Function

``` 

![12](https://user-images.githubusercontent.com/53209422/106386438-f77bed80-63fa-11eb-86eb-da409ea138ba.jpg)




> Click Alt + F11 to go back to the spreadsheet.

15. Now select the cell just below the XMIN and type   
```
=Convert_Degree(C2)
``` 
in the formula box and click **ENTER**.

**Change the C2 with the cell name contains the first decimal value of xmin**.

![13](https://user-images.githubusercontent.com/53209422/106386440-fa76de00-63fa-11eb-9e40-02a596d1e14f.jpg)

16. Drag the  +  sign in the right bottom corner of the same cell to the last valued row.


17. Repeat the same for XMAX, YMIN and YMAX. 


![14](https://user-images.githubusercontent.com/53209422/106386445-fd71ce80-63fa-11eb-8473-5563365044f4.jpg)

> Note: Remember to change the cell value in the Step 15 in next steps.

18. Save the file in Excel macro enabled format (.xlxm). By selecting this format, the module we added in VBA will be attached with the workbook. 

![15](https://user-images.githubusercontent.com/53209422/106386448-006cbf00-63fb-11eb-974f-489896d06ba2.jpg)

### Things to be noted

If the required output is in decimal values, then up to step 9 is enough. Those processes are also possible in ArcGIS software. 


<a href="https://gpay.app.goo.gl/pay-sCjOGpyBhXM" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A " style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>


































