

---
title: How to add audio in hugo static site using a shortcode.
description: A shortcode for embedding an audio file in  hugo static sites
date: 2020-04-03
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1","h2", "h3", "h4"]
tags: ["shortcodes","hugo"]
categories: ["tutorials"]   
---




This is a simple tutorial on how to add audio files in hugo static sites via shortcodes. Just follow these steps

# STEP 1

Create a file _audio.html_ in _layouts/shortcodes_ folder.

# STEP 2

 Add following lines of code in the newly created _audio.html_ file and save.
 
# STEP 3

Save the required audio file in _/static/xxx_ . For the demonstrative purpose I put the sample audio file in _/static/posts/shortcodes/_ (That's where I put my static files for shortcode demonstrating articles). That's all you are ready to go.



{{<gist arungop 3a15a2a99b7fffbf0176e8dcdb803db6 >}}



{{< audio mp3="/audio/filename.mp3" >}}


**SONG: _Stand By (#CWC19)_**


**CREDITS: _SME (on behalf of Black Butter); BMI - Broadcast Music Inc., UNIAO_**


**ARTIST: _[LORYN](https://www.youtube.com/channel/UCNJwoRlTn6tFY7oYO6AXgGQ)_** 




