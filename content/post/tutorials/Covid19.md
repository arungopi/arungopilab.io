---
title: COVID 19 തൽസമയ ഭൂപടം   
description: ദിവസേനയുള്ള വിവരങ്ങൾ ഭൂപടത്തിൽ  .
date: 2020-03-06
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1","h2", "h3", "h4"]
tags: ["covid 19"]

---

ലോകത്തെ പിടിച്ചു കുലുക്കുന്ന ഒരു മഹാമാരിയായി COVID19 മാറിക്കഴിഞ്ഞിരിക്കുന്നു. 

> The 2019–20 coronavirus outbreak is an ongoing global outbreak of coronavirus disease 2019 that has been declared a Public Health Emergency of International Concern. It is caused by the SARS-CoV-2 coronavirus, first identified in Wuhan, Hubei, China. [1]


# തൽസമയ ഭൂപടങ്ങൾ

തൽസമയ ഭൂപടങ്ങൾ നമ്മൾ പ്രകൃതി ദുരന്തങ്ങളിൽ ഉപയോഗിച്ചു കണ്ടിട്ട്ള്ളതാണു ഇപ്പോൾ പകർന്നു വ്യാപിക്കുന്ന COVID19 അതിന്റെ വ്യാപന നിരക്ക് ,അതിന്റെ സഞ്ചാരപാത ഒക്കെ ഈ ഭൂപടത്തിൽ കൂടെ മനസിലാക്കുവാൻ കഴിയും.



## Coronavirus COVID-19 Global Cases by Johns Hopkins CSSE


കൂട്ടത്തിൽ ആധികാരികവും മാപ്പിങ് Desktop/Mobile പ്ലാറ്റ്ഫോർമിനും ഒരുപോലെ ലഭ്യമാക്കിയിരിക്കുന്നതു Coronavirus COVID-19 Global Cases by Johns Hopkins CSSE ആണ്. Geospatial ഭീമൻമാർായ ESRI ആണ് ഈ മാപ്പിങ് Johns Hopkins യൂണിവേർസിറ്റ്യുമായി സഹകരിച്ച് നടത്തുന്നത് .
	
### Features - Desktop

![General view - Coronavirus COVID-19 Global Cases by Johns Hopkins CSSE](/Covid19/overall.jpg)

![Cumulative view](/Covid19/fullscreen_cumulative.jpg)

![Active cases](/Covid19/active_cases.jpg)

![Interactive plot -Cumulative](/Covid19/graph.jpg)
	
![Interactive plot -Daily](/Covid19/graph_daily.jpg)
		


	

**[Visit now](https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)**

### Features - Mobile

![Mobile version](/Covid19/mobile.jpg)
		
![Data counter](/Covid19/mobile1.jpg)
		


**[Visit Mobile version ](https://www.arcgis.com/apps/opsdashboard/index.html#/85320e2ea5424dfaaa75ae62e5c06e61)**
	
### Data Sources

ഈ തൽസമയ മാപ്പിങ്ങിൻനായി ഉപയോഗിക്കുന്ന വിവര സ്ത്രോതസ്സുകൾ:

* [WHO](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports)
* [CDC](https://www.cdc.gov/coronavirus/2019-ncov/index.html)
* [ECDC](https://www.cdc.gov/coronavirus/2019-ncov/index.html)
* [NHC](https://www.cdc.gov/coronavirus/2019-ncov/index.html)
* [DXY](https://3g.dxy.cn/newh5/view/pneumonia?scene=2&clicktime=1579582238&enterid=1579582238&from=singlemessage&isappinstalled=0)




[1]: [https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_outbreak]
 
