---
title: Movie barcodes of Dileesh Pothan movies
author: Arun Gopinath
date: '2022-12-17'
draft: False
slug: []
categories:
  - fun
  - tutorials
tags:
  - python
  - movie
  - barcode
---

# Movie barcodes of Dileesh Pothan movies

[Dileesh Pothan](https://www.imdb.com/name/nm4584004/) is already a rocking star in the Malayalam movie industry for his outstanding movies. Within just three films, he already owns a place in the minds of movie buffs. Subtle details and realistic presentation of stories are popularly known as Pothettan brilliance.His works with [Fahad Faasil](https://www.imdb.com/name/nm1335704/?ref_=tt_cl_t_1) are a treat to watch. Apart from directorial skills, he is also known for his produced movies.

In this post, I will show you three barcodes of Pothen’s movie. Each line of the barcode represents the average colour of the frame. These frames are stacked to form a barcode.


## Can you guess the movies ?

### A

![](images/mah.JPG)

### B

![](images/joji.jpg)
### C

![](images/tmd.jpg)

### Answers 

Movies are:


C: [Thondimuthalum Driksakshiyum](https://www.imdb.com/title/tt5906392/?ref_=nm_knf_t_2) 


B: [Joji](https://www.imdb.com/title/tt13206926/?ref_=nm_knf_t_1)


A: [Maheshinte Prathikaaram](https://www.imdb.com/title/tt4851630/?ref_=nm_knf_t_3) 

## What's next ?

If you are a movie buff, then you can recollect scenes by analysing the barcode. Like the one where the Chachan character of the movie Maheshinte Prathikaram develops his photographs in a dark room with red lights. Yes, just goosebumps...


> All movie barcodes are made using python code created by [Andrew Campbell](https://github.com/andrewdcampbell/movie-barcodes)
