---
title: "EIA and mailto  "
date: 2020-08-13T09:05:59+05:30
description: "Creating a mailto link for a webpage"
draft: false
show_toc: true
tocLevels: ["h1","h2", "h3", "h4"]
tags:
- mailto
series:
-
categories:
- tutorials

---

# ഇത് അതല്ല

വളരെ വൈകി വന്ന വിവേകം പോലെയാണ് EIA- 2020നു എതിരെ ഉയർന്ന പ്രതിഷേധം. ഓഗസ്റ്റ് 11 നു അവസാന തിയതി എന്ന് വന്നപ്പോൾ, സോഷ്യൽ  മീഡിയയും മാധ്യമങ്ങളും എന്തിനു രാഷ്ട്രീയ നേതൃത്വം വരെ ഉണർന്നു.  ഇവിടെ EIA 2020 യെ പറ്റി അല്ല പകരം അവിടെ തരംഗമായ [https://environmentnetworkindia.github.io/](https://environmentnetworkindia.github.io/) എന്നൊരു സൈറ്റ് നിങ്ങളിൽ പലരും കണ്ടിട്ടുണ്ടാകും. ഒറ്റ ക്ലിക്കിൽ ഇതാ, അതാതു മന്ത്രാലയത്തിന് മെയിൽ അയയ്ക്കാൻ ഇന്ദ്രജാലം പോലെ നിങ്ങളെ കൊണ്ട് പോകുന്നു. മലയാളം, ഇംഗ്ലീഷ് എന്ന് വേണ്ട ഒട്ടുമിക്ക ഭാഷകളിലും മെയിൽ തയ്യാർ. ഇവിടെ നിങ്ങളെ അതിനു സഹായിച്ച ഒരു ടെക്‌നിക്‌ ആണ് **mailto** (ഇതേ പേരിൽ ഒരു സ്ഥലം അങ്ങ് ഇറ്റലിയിൽ ഉണ്ട് , ഇത് അതല്ല ).


![web](https://user-images.githubusercontent.com/53209422/98445620-9b2c6900-213e-11eb-8911-95b18ffde3ad.png)

# mailto 


mailto എന്നത് ഇമെയിലുകൾക്കു ഉള്ള ഒരു   Uniform Resource Identifier (URI) സ്കീം ആണ്. ഇത്  വെബ്സൈറ്റുകളിൽ  ഹൈപ്പർലിങ്ക് ഉണ്ടാക്കുകയും, അതുവഴി  ഒരു നിർദ്ദിഷ്ട വിലാസത്തിലേക്ക് ഒരു ഇമെയിൽ അയയ്ക്കാൻ ഉപയോക്താക്കളെ  അനുവദിക്കുകയും ചെയ്യുന്നു.  പ്രധാനപ്പെട്ട ഉപയോഗം കോപ്പി / പേസ്റ്റ് ചെയ്യാതെ കാര്യം നടക്കുന്നു എന്നതാണ്.[^1]


# Creating a mailto link

ഒരു * ഡോക്യൂമെന്റിൽ * ലിങ്ക് തയ്യാറാക്കാൻ വളരെ ലളിതമായി ഒരു ഹൈപ്പർലിങ്ക് ഉണ്ടാക്കുക എന്നതാണ് .




```html
 <a href="eia2020-moefcc@gov.in">Send email</a> 
```




ഇത് നിങ്ങളെ നിങ്ങളുടെ മെയിൽ ക്ലൈന്റിൽ ( ജിമെയിൽ ), മെയിൽ കിട്ടേണ്ട ആളുടെ വിലാസവുമായി (mailto:eia2020-moefcc@gov.in) ഓപ്പൺ ആകും.

ഹെൽഡറുകൾ അതായതു സബ്ജക്ട് മുതലായവ ഉൾപ്പെടുത്തി ഇവ വിപുലീകരിക്കാൻ കഴിയും. വാക്കുകൾക്കിടയിൽ സ്പേസ് അനുവദിനീയമല്ല. പകരം %20 എന്നത് സ്പേസിനെ സൂചിപ്പിക്കുന്നു.



```html
 <a href=" mailto:mefcc@gov.in,eia2020-moefcc@gov.in,connect@mygov.nic.in,secy-moef@nic.in?bcc=eia@hi2.in&?subject=Withdraw%20EIA%20draft&body=To%20whom%20it%20may%20concern ">Send email</a> 
```


 <a href=" mailto:mefcc@gov.in,eia2020-moefcc@gov.in,connect@mygov.nic.in,secy-moef@nic.in?bcc=eia@hi2.in&?subject=Withdraw%20EIA%20draft&body=To%20whom%20it%20may%20concern ">Send email</a> 
 

ഒന്നിലധികം വിലാസങ്ങൾ ചേർക്കാൻ **, (comma)** ഉപയോഗിക്കണം.



![output](https://user-images.githubusercontent.com/53209422/98445617-99fb3c00-213e-11eb-9afd-1bb580628bc6.png)



# mailto link generator

വളരെ വേഗത്തിൽ ഓൺലൈൻ ആയി **mailto** ലിങ്കുകൾ നിർമിക്കാൻ ഈ വെബ്സൈറ്റുകൾ ഉപയോഗിക്കാവുന്നതാണ്.


* [https://mailtolink.me/](https://mailtolink.me/)

* [https://e-mailer.link/en/](https://e-mailer.link/en/)

* [https://htmlstrip.com/mailto-generator](https://htmlstrip.com/mailto-generator)


[^1]: https://en.wikipedia.org/wiki/Mailto


