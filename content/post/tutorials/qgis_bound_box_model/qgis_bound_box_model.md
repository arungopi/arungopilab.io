---
title: QGIS model to quickly calculate bound box of all polygons in a shapefile
author: Arun Gopinath
date: '2021-12-23'
slug: []
categories:
  - tutorials
tags:
  - GIS
  - QGIS
---
Calculating the boundary box of a large number of polygons in a shapefile in QGIS is discussed earlier in [this post](https://arungopi.gitlab.io/post/tutorials/bound-extend/). The process itself is tedious and required a support from Microsoft Office. There has been numerous updates arrived in QGIS and several algorithms were incorporated. Keeping this in mind I developed a QGIS model which can create an output with a simple click.

[Download this Model](https://raw.githubusercontent.com/arungop/QGIS_boundbox/main/Boundary_box.model3) and save it in suitable location.

> More on this [github repo](https://github.com/arungop/QGIS_boundbox)

Open the shapefile in QGIS and follow these steps.

![](https://user-images.githubusercontent.com/53209422/147188898-8031d826-b981-4628-9731-c6d8bd1f6820.png)

Now, select *model tool box* icon and select Open *existing model*. In the window that appears select above saved file and click apply.



![](https://user-images.githubusercontent.com/53209422/147188884-17c280bc-f68c-4c6d-a7ef-19e373d9807d.png)

After that in *processing toolbox*, we can see our **Boundarybox model** appears in Models tool.

Double click the **Boundarybox icon** and in the dialog box that appears **unselect** all the options denoting *"Open output file after running algorithm"*
**except** for the last section [Bound (optional)] (see picture). 

![](https://user-images.githubusercontent.com/53209422/147188891-c8da0f85-0ad0-4c58-8aae-0593088c7b8b.png)

You can provide suitable name and save the file in the last option. 


Click **Run**.




If you not saved the file in last option, then right click the temporary file named *Bound* **(Default name)** and save it in your required location.

![](https://user-images.githubusercontent.com/53209422/147188892-d7528b5f-e8e4-4bca-91b2-e3f6ccf76ace.png)

* Check attribute table. 

![](https://user-images.githubusercontent.com/53209422/147188895-f23694bb-c7e7-4fa6-9caa-6bf4f6afa830.png)
Hope it hepls.

> Note: This model only works after QGIS 3.16

