---
title: Convert a bulk of pdf files into images in just two lines of code (Only for linux users)
author: Arun Gopinath
description: A linux tutorial on converting a large number of pdf files into images in just two lines of code. Here we used pdftoppm function. This is the fastest approach available for linux ecosystem.
date: '2021-07-31'
slug: []
categories:
  - tutorials
tags:
  - linux
  - pdftojpg
---
# Just two lines of code for linux users

Let's say you have a folder consists of hundreds of pdf files. Your job is to convert it into image format (jpg or png). If you are in a Linux environment, then the solution is pretty simple. **Just two lines of code!**

In the *file manager* open the folder containing pdf files. *Right-click and select Open terminal (here)*.  Once inside that directory,then you can type or copy and paste the following command in the terminal and press enter:

```
find . -maxdepth 1 -type f -name '*.pdf' -exec pdftoppm -jpeg {} {} \;
```

Here we used a cool tool called [pdftoppm][^1]. In the above line of code, we use 'find' to search for pdf files and set it to '-maxdepth' and convert it to jpeg format. Done, you will find the converted jpg files in the same directory alongside the old pdf files. But it's a mess to identify, isn't it? Make use of 'mkdir' and 'mv' functions to move these images to a new folder:
```
mkdir jpg_files && mv *.jpg jpg_files/
```
## There are other methods too, but...

There are other ways to do this especially with [ImageMagick][^2] but this method is efficient and lossless method for practical use.

[^1]:https://linux.die.net/man/1/pdftoppm
[^2]:https://askubuntu.com/questions/150100/extracting-embedded-images-from-a-pdf/1187844#1187844

<a href="https://gpay.app.goo.gl/pay-sCjOGpyBhXM" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A " style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>
