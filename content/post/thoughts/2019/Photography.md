

---
title: Photographic experiments
author: Arun Gopinath
date: '2019-12-21'
categories:
  - random thoughts
tags:
  - photography  
---



Photography is the art of freezing the present for the future. The creative side of the photographer makes the photographs a valuable property. Basically one can argue that a good photograph is born irrespective of the device it shoot. But technically quality stuffs need quality gadgets. Checkout my latest photographs in Gallery section

![mush](https://user-images.githubusercontent.com/53209422/98466033-f6b82e80-21f2-11eb-981b-96952b39c470.jpg)


