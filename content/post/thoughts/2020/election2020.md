---
title: 'പെരുങ്കടവിള തദ്ദേശ തിരഞ്ഞെടുപ്പിന്റെ ചില സ്ഥിതി വിവര കണക്കുകൾ '
author: Arun Gopinath
date: '2020-12-16'
description: A fun analysis of LSG election of perumkadavila panchayat 2020 using google sheet tools.
show_toc: true
draft: false
categories:
  - random thoughts
tags: 
  - lsg election
  - infographics
---




# പെരുങ്കടവിള തദ്ദേശ തിരഞ്ഞെടുപ്പിന്റെ ചില സ്ഥിതി-വിവര കണക്കുകൾ 

2020-ലെ കേരളം തദ്ദേശ തിരഞ്ഞെടുപ്പിന്റെ ആവേശം ഉൾക്കൊണ്ട് എന്റെ പഞ്ചായത്തായ പെരുങ്കടവിളയിലെ ചില രസകരമായ തിരഞ്ഞെടുപ്പ് വസ്തുതകളാണ് ഈ ലേഖനത്തിൽ. ഈ അന്വേഷണത്തിൽ പഞ്ചായത്തിലെ 16 വാർഡുകളുടെ വിവരങ്ങൾ ആണ് ഉൾക്കൊള്ളിച്ചിരിക്കുന്നത്. ത്രിതല ഭരണ സംവിധാനത്തിൽ മുകൾ തട്ടായ ബ്ലോക്ക് ഡിവിഷനും ജില്ലാ ഡിവിഷനും ഇതിൽ ഉൾപ്പെടുത്തിയിട്ടില്ല .


Total available votes:  <span style="color:red">20495</span>

![Perumkadavila_panchayat](https://user-images.githubusercontent.com/53209422/102505505-166f2c00-40a8-11eb-94fa-01d8d6c68122.jpg)

[പഞ്ചായത്ത് ഭൂപടം](https://osm.org/go/ym33fRB?m=&relation=11492796)

[വാർഡുകൾ ഭൂപടം - സർക്കാർ വക](http://sec.kerala.gov.in/images/maps/G01010.pdf)



## സ്ഥാനാർത്ഥികൾ - ചില രസകരമായ വിവരങ്ങൾ 

വിവരശേഖരണം കേരളം സ്റ്റേറ്റ് ഇലെക്ഷൻ കമ്മീഷന്റെ വെബ്‌സൈറ്റ് വഴി ഡൌൺലോഡ് ചെയ്തെടുക്കൽ ആയിരുന്നു ആദ്യപടി. പതിനാറു വാർഡിലെയും സ്ഥാനാർത്ഥികൾ അവരുടെ - പേര് , വിലാസം , പ്രായം , പാർട്ടി, ചിഹ്നം, ലിംഗം തുടങ്ങിയ അടിസ്ഥാന വിവരങ്ങൾ ആണ് അതിൽ കാണാനാവുക. ഓരോ വാർഡിലെയും വിവരങ്ങൾ ശേഖരിച്ചു പഞ്ചായത്തിന്റെ മുഴുവൻ വിവരങ്ങൾ ചുവടെ.
 
{{< gist arungop 68d9a7311c85ba6fab51552b1902e78c "candidates.md">}}

### ആൺ-പെൺ അനുപാതം

മൊത്തം 63 സ്ഥാനാർഥികളിൽ 31 സ്ത്രീകളും ബാക്കി 32 പുരുഷന്മാരും ആണ് മത്സര രംഗത്തുള്ളത് .അതുകൊണ്ട് തന്നെ ആൺ - പെൺ അനുപാതം <span style="color:red">1.032 : 1</span> ആണ്. 

### പാർട്ടി അടിസ്ഥാനത്തിൽ 

![Candidates - Party distribution](https://user-images.githubusercontent.com/53209422/101283633-b48a0900-3801-11eb-8be3-6ef9e6a9f657.png)

മൊത്തം സ്ഥാനാർഥികളിൽ 27 % സ്വത്രതന്മാരാണ്, അതായതു 17 പേർ. പാർട്ടി അടിസ്ഥാനത്തിൽ 16 സ്ഥാനാർത്ഥികളെ നിറുത്തിയ കോൺഗ്രസ് പാർട്ടി രണ്ടാമതാണ്. ബിജെപി 15 സ്ഥാനാർത്ഥികളെ മുന്നോട്ട്വയ്ക്കുമ്പോൾ, സിപിഎം 12 ഉം സിപിഐ 2 ഉം ജനതാദൾ(സ്) ഒരു സ്ഥാനാർഥിയെയും മത്സരരംഗത്തു നിറുത്തിയിരിക്കുന്നു.


### ഏറ്റവും കൂടുതൽ സ്ഥാനാർത്ഥികൾ ഏതു വാർഡിൽ ?
<span style="color:blue">തത്തിയൂർ വാർഡിൽ: 6 സ്ഥാനാർത്ഥികൾ ! </span>
 

വാർത്തകളിൽ വരുന്നതു പോലെ പത്തും ഇരുപതും സ്ഥാനാർത്ഥികൾ  തദ്ദേശ സ്വയംഭരണ തിരഞ്ഞെടുപ്പിൽ കാണാൻ കഴിയാറില്ല. മറ്റു ചില രസകരമായ വിവരങ്ങൾ നോക്കാം. മൂന്നു വാർഡുകളിൽ 5 പേരും , ബാക്കി ഉള്ള വാർഡുകളിൽ 6 വാർഡിൽ നാലുപേർ വീതവും ബാക്കിയുള്ള 6 എണ്ണത്തിൽ 3 സ്ഥാനാർത്ഥികൾ വീതവും ആണ്.

{{< gist arungop c2b277ef392db8cc98c4e52df9f5682d "candidates_count.md">}}

### മൊത്തം എത്ര ചിഹ്നങ്ങൾ ?

<span style="color:blue">12</span>

### സ്വാതന്ത്രന്മാർക്ക് ഏറ്റവും കൂടുതൽ ഇഷ്ടമുള്ള ചിഹ്നം ?
<span style="color:blue">ഫുട്ബോൾ - 6 പേർക്ക് </span>


തൊട്ടടുത്ത് തന്നെ ആപ്പിൾ 5 പേർക്ക് ലഭിക്കുകയുണ്ടായി. ഇത്കൂടാതെ, ഓട്ടോറിക്ഷ - 3, മെഴുകുതിരി, ക്രിക്കറ്റ്ബാറ്റ്, കുടം, പമ്പരം എന്നിവ ഓരോ പേർക്കും ലഭിക്കുകയുണ്ടായി.

![Count of Symbol](https://user-images.githubusercontent.com/53209422/101284180-b43f3d00-3804-11eb-80cf-9d709b55fb3d.png)

### സ്ഥാനാർത്ഥികളുടെ ശരാശരി പ്രായം എത്ര ?



<span style="color:blue">**46**</span>


### പ്രായം കൂടിയ സ്ഥാനാർഥി ? പ്രായം കുറഞ്ഞ സ്ഥാനാർഥി ?



| Male - Most    | 70 | Soman Nair K | Aruvikkara   |
|----------------|----|--------------|--------------|
| Female - Most  | 63 | Chandrika C  | Thripalavoor |
| Male - Least   | 32 | S S Sreeragh | Thathamala   |
| Female - Least | 25 | Lavanya L K  | Ancode       |

### ഏതു ചിഹ്നത്തിൽ/പാർട്ടിയിൽ ആണ് ശരാശരി പ്രായം കൂടുതൽ ?

ശരാശരി പ്രായം നോക്കിയാൽ പ്രായം കൂടിയ സ്വതന്ത്രൻ മുന്നിൽ വരാനുള്ള സാധ്യതയാണ് കൂടുതൽ. ഇവിടെയും, കുടവും (58), ക്രിക്കറ്റ് ബാറ്റും (57) ആണ് മുന്നിൽ. പ്രമുഖ പാർട്ടികളിൽ കോൺഗ്രസിന്റെ സ്ഥാനാർഥികളിലെ ശരാശരി പ്രായം 48.75 ആണ്. ഏറ്റവും കുറവ് സിപിഐഎം സ്ഥാനാത്ഥികളുടെതാണു : 45.75. ഒറ്റ സ്ഥാനാർത്ഥിയുള്ള ജനതാദൾ(സ്) ന്റെ ശരാശരി പ്രായം ആണ് പട്ടികയിൽ ഏറ്റവും ചെറുപ്പം: 33.



![Average age vs Symbol](https://user-images.githubusercontent.com/53209422/101285153-acce6280-3809-11eb-9017-55d01558cd37.png)


###  ജനറൽ വാർഡിലെ വനിത സ്ഥാനാർത്ഥികൾ എത്ര? ആരൊക്കെ ?

<span style="color:blue">**2 സ്ഥാനാർത്ഥികൾ**</span>

തത്തിയൂർ വാർഡിലെ ശ്രീമതി G വത്സലകുമാരിയും, അരുവിപ്പുറം വാർഡിൽ ശ്രീമതി പ്രതിഭ R - എന്നിവരാണ് ജനറൽ വാർഡിലെ സ്ത്രീ സ്ഥാനാർത്ഥികൾ


### സ്ഥാനാർത്ഥികളുടെ ശരാശരി പ്രായം - വാർഡ് അടിസ്ഥാനത്തിൽ


<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=460942706&amp;format=interactive"></iframe>


### ഓരോ വാർഡിലെയും മൊത്തം വോട്ടർമാർ എത്ര ?



<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=627534314&amp;format=interactive"></iframe>


##  Election results 

National Informatics Centre(NIC) Network തയ്യാറാക്കിയ [Trend kerala](http://trend.kerala.gov.in/views/index.php) എന്ന website ആണ് ആധികാരികമായി ഇലക്ഷൻ വിവരങ്ങൾ പൊതുജനങ്ങൾക്കും മാധ്യമങ്ങൾക്കും ലഭ്യമാക്കിയത്. അടിസ്ഥാന വിവരങ്ങൾ ശേഖരിച്ചു, അരിച്ചെടുത്ത ചില വസ്തുതകൾ ചുവടെ.

### പോളിങ് ശതമാനം എത്ര ?

മൊത്തം ഉണ്ടായിരുന്ന 20495 വോട്ടിൽ <span style="color:blue">16940</span> വോട്ടാണ് രേഖപ്പെടുത്തിയത്. അതായതു <span style="color:red">82.65% </span> ആണ് 2020 ലെ പോളിങ് ശതമാനം.




### പോളിങ് ഏറ്റവും കൂടുതൽ നടന്നതെവിടെ ?

| Ward          | Polling Percentage |
|---------------|------------------------|
| Aruvikkara    | 88.66%                 |
| Thathiyoor    | 87.30%                 |
| Pazhamala     | 87.17%                 |
| Alathoor      | 85.40%                 |
| Vadakara      | 84.63%                 |
| Pulimancode   | 84.16%                 |
| Thripalavoor  | 82.85%                 |
| Palkulangara  | 82.59%                 |
| Anamugham     | 82.30%                 |
| Chulliyoor    | 81.30%                 |
| Marayamuttom  | 80.85%                 |
| Thathamala    | 80.13%                 |
| Perumkadavila | 80.07%                 |
| Ayiroor       | 79.64%                 |
| Aruvippuram   | 77.45%                 |
| Ancode        | 77.39%                 |

* പട്ടികയിൽ കാണുന്നതുപോലെ അരുവിക്കര വാർഡിലാണ് ഏറ്റവും കൂടുതൽ പോളിങ് നടന്നത്. 

* തൊട്ട് പിന്നിൽ തത്തിയൂർ വാർഡ്. ഏറ്റവും കുറച്ചു പോളിങ് നടന്നത് അങ്കോട് വാർഡിലാണ്. 



### വിജയികൾ

| Candidate                | Majority | Party       | Ward          | Gender |
| ------------------------ | -------- | ----------- | ------------- | ------ |
| Sujith C                 | 352      | CPI(M)      | Aruvippuram   | M      |
| Jayachandran K S         | 221      | BJP         | Aruvikkara    | M      |
| S Bindhu                 | 132      | CPI(M)      | Marayamuttom  | F      |
| Rajikumar (Murukan)      | 124      | CPI(M)      | Anamugham     | M      |
| Suchithra V A            | 120      | BJP         | Ayiroor       | F      |
| Kanakkode Balaraj        | 120      | CPI         | Palkulangara  | M      |
| S S Sreeragh             | 120      | BJP         | Thathamala    | M      |
| Dhanya P Nair            | 112      | INC         | Ancode        | F      |
| Mini Prasad              | 110      | Independent | Thripalavoor  | F      |
| Ambalatharayil Gopakumar | 107      | INC         | Pulimancode   | M      |
| Vineetha Kumari          | 58       | BJP         | Alathoor      | F      |
| Manjusha Jayan           | 53       | INC         | Vadakara      | F      |
| Snehaletha R             | 50       | CPI(M)      | Pazhamala     | F      |
| S Surendran              | 50       | CPI(M)      | Perumkadavila | M      |
| Kakkanam Madhu           | 50       | Independent | Thathiyoor    | M      |
| Vimala M                 | 39       | CPI(M)      | Chulliyoor    | F      |

* ഏറ്റവും കൂടുതൽ ഭൂരിപക്ഷം നേടി വിജയിച്ചത് അരുവിപ്പുറം വാർഡിലെ സിപിഐഎം സ്ഥാനാർഥി ആയിരുന്ന സുജിത് സി ആണ്, 352 വോട്ടിനു.
* ഏറ്റവും കുറച്ചു ഭൂരിപക്ഷം നേടി വിജയപഥം നേടിയത് സിപിഐഎം ന്റെ തന്നെ സ്ഥാനാർഥി ആയിരുന്ന ചുള്ളിയൂർ വാർഡിലെ വിമല എം, 39 വോട്ടിനു !
* കൃത്യം 50, 120 എന്നീ  വോട്ടു ഭൂരിപക്ഷം കിട്ടിയത് 3  പേർക്ക്  വീതമാണ് !!



### LDF നു മുൻത്തൂക്കം


<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=2132077453&amp;format=interactive"></iframe>


63 സ്ഥാനാർഥികളിൽ തുടങ്ങിയ മാമാങ്കം 8 സ്ത്രീകളും 8 പുരുഷന്മാരും അടങ്ങുന്ന വിജയികളിൽ അവസാനിച്ചിരിക്കുകയാണ്. 
പാർട്ടി അടിസ്ഥാനത്തിൽ ഉള്ള സ്ഥിതിവിവരണകണക്ക് താഴെ.


<iframe width="607" height="376" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=1202577671&amp;format=interactive"></iframe>





### പഞ്ചായത്തിൽ ഏറ്റവും കൂടുതൽ വോട്ട്നേടിയ പാർട്ടി ?

ബിജെപി 4714 വോട്ട് നേടിയപ്പോൾ, 4572 വോട്ട് നേടി സിപിഐഎം. LDF മുന്നണി സിപിഐ യുടെ 754 വോട്ട് കൂടി ചേരുന്നതോടുകൂടി ഒന്നാംസ്ഥാനം നേടുന്നു. INC ( കോൺഗ്രസ് ) 4160 വോട്ട് നേടി മൂന്നാംസ്ഥാനത്താണ് ഫിനിഷ് ചെയ്തത്. മൊത്തം ഉണ്ടായിരുന്ന 17 സ്വതന്ത്രർ ആകെക്കൂടി 2241 വോട്ട് നേടിയത് നിർണായകമായി.

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=566873702&amp;format=interactive"></iframe>


### വിജയികൾ എത്ര വോട്ട് നേടി ? ആർക്കാണ് കൂടുതൽ വോട്ട് കിട്ടിയത് ?

<iframe width="699" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=1908515996&amp;format=interactive"></iframe>

ഓരോ വിജയികൾക്കും ലഭിച്ച വോട്ടാണ് മുകളിലെ ഗ്രാഫിൽ കാണിച്ചിരിക്കുന്നത്.

ഏറ്റവും കൂടുതൽ വോട്ട് നേടിയ സ്ഥാനാർഥിക്കു തന്നെയാണ് ഏറ്റവും കൂടുതൽ ഭൂരിപക്ഷവും !
വിജച്ചു വന്നിട്ടുള്ള രണ്ടു സ്വത്രന്മാർക്കാണ് വിജയികളിൽ ഏറ്റവും കുറച്ച വോട്ട് നേടിയത് എന്നത് മറ്റൊരു രസകരമായ വസ്തുത.


### ഈ തിരഞ്ഞെടുപ്പിൽ പെരുങ്കടവിള പഞ്ചായത്തിൽ ഏറ്റവും കുറച്ചു വോട്ട് കിട്ടിയതാര്ക്കൊക്കെ ?

ഏറ്റവും കുറച്ചു വോട്ട് കിട്ടിയ ആദ്യ പത്തുപേർ.

| Candidate           | Polled votes |
|---------------------|--------------|
| R Babu              | 2            |
| Shaji M S           | 4            |
| B T Rama            | 16           |
| Shibu R B (Unni)    | 20           |
| Jolly John          | 23           |
| Lathika S           | 34           |
| C Mohanakumar       | 62           |
| Binukumar R         | 74           |
| Marayamuttom Rajesh | 77           |
| Samkutty            | 84           |

### പുത്തൻ പഞ്ചായത്ത് ഭരണസമിതിയുടെ ശരാശരി പ്രായം ?

<span style="color:green">43.94</span> ആണ് ഇത്തവണത്തെ പഞ്ചായത്ത് ഭരണസമിതിയുടെ ശരാശരി പ്രായം. 
*  പുരുഷന്മാരുടെ ശരാശരി പ്രായം <span style="color:purple">45.13</span> ഉം സ്ത്രീകളുടേത് <span style="color:red">42.75</span> ഉം ആണ്. 

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubchart?oid=2119512777&amp;format=interactive"></iframe>

### ഏറ്റവും പ്രായം കുറഞ്ഞ വിജയി ? കൂടിയത് ?

| Male - Most    |<span style="color:red"> 56</span> | Kanakkode Balaraj | Palkulangara |
|----------------|----|-------------------|--------------|
| Female - Most  | <span style="color:red">55</span> | Vimala M          | Chulliyoor   |
| Male - Least   | <span style="color:orange">32</span> | S S Sreeragh      | Thathamala   |
| Female - Least | <span style="color:orange">35</span> | Suchithra V A     | Ayiroor      |

* ഏറ്റവും ചെറുപ്പക്കാരനായ വിജയി S S ശ്രീരാഗ് ( തത്തമല )
* പ്രായം കൂടിയ വിജയി കനകക്കോട് ബാൽരാജ് (പാൽക്കുളങ്ങര)
* സ്ത്രീകളിൽ പ്രായം കുറവ് - സുചിത്ര വി എ (അയിരൂർ), പ്രായം കൂടുതൽ - വിമല എം (ചുള്ളിയൂർ)

### സ്വതന്ത്രർ തീരുമാനിക്കും !

രണ്ടു സ്വതന്ത്രർ ആണ് വിജയിച്ചത്. ഒരു വനിതയും ഒരു പുരുഷനും.


* <span style="color:red">കാക്കണം മധു</span> - തത്തിയൂർ വാർഡ് - ആപ്പിൾ ചിഹ്നം



* <span style="color:red">മിനി പ്രസാദ്
</span> - തൃപ്പലവൂർ വാർഡ് - ഫുട്ബോൾ ചിഹ്നം


ഭൂരിപക്ഷത്തിനു 16 ൽ 9 വേണം എന്നിരിക്കേ, സ്വതന്ത്രർ തീരുമാനിക്കും പഞ്ചായത്ത് ഭരണം !

## ഈ ലേഖനത്തിനു വേണ്ടി ശേഖരിച്ച തിരെഞ്ഞെടുപ്പ് വിവരങ്ങൾ

[തിരെഞ്ഞെടുപ്പ് വിവരങ്ങൾ](https://docs.google.com/spreadsheets/d/e/2PACX-1vSsgNTep0F7JTXnSeps6-r79MpXOUhtrSBnsX79NCX6eWG9Yrz5QNfRjysX-XPAKORfe9cc2JFI84dQ/pubhtml?gid=0&single=true)


**Support**


[Buy me a കട്ടൻ ചായ](https://gpay.app.goo.gl/pay-sCjOGpyBhXM)

or 

<a href="https://gpay.app.goo.gl/pay-sCjOGpyBhXM" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A " style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>


