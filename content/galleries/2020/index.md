---
title: "Pandemic times"
date: 2020-08-10T20:39:13+02:00
draft: false
tags:
- photography
categories:
- gallery
---

# Year of Uncertainity

“Out of the void and vastness of the cosmos, life emerges; audacious, improbable. You and I are here. No other miracle is needed.”
― John Mark Green

***



{{<gallery_row folder="images/" uniqueID="3" showAlt=true gap=5 height=300 >}}
