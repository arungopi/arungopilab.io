---
title: "Moments to cherish"
date: 2019-12-31T20:39:13+02:00
draft: false
tags:
- photography
categories:
- gallery
---

# Joyful times

“I have realized that the past and future are real illusions, that they exist in the present, which is what there is and all there is.”
― Alan Wilson Watts

***

{{<gallery_row folder="img/" uniqueID="3" showAlt=true gap=5 height=300 >}}
