---
title: "2023 in Snapshots: A Random Ride Through Memorable Moments"

date: 2023-12-16T00:00:00+05:30
draft: false
tags:
- photography
categories:
- gallery
---


# 2023 in Pics: My Unplanned Year in a Nutshell!

Take a chill scroll through my year-end photogallery. No filters, no drama – just the random bits that made my year uniquely mine.

Life's random, and so is this gallery. Let's keep it real, keep it simple, and enjoy the ride together. Here's to the memories, the chaos, and the surprisingly cool stuff that made 2023 one for the books!"


> "As the sentinel of the frontier of a new civilization, it is for you to cross the forbidden - I am a wanderer searching for the artifacts of an ancient curiosity." - **S K Pottakkatt**

***



{{<gallery_row folder="img/" uniqueID="3" showAlt=true gap=5 height=400 >}}

