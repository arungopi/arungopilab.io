---
title: "Roller Coaster ride"
date: 2022-12-18T00:00:00+05:30
draft: false
tags:
- photography
categories:
- gallery
---

# Roller Coaster ride

“With peaks of joy and valleys of heartache, life is a roller coaster ride, the rise and fall of which defines our journey. It is both scary and exciting at the same time.”

- Sebastian Cole

***



{{<gallery_row folder="images/" uniqueID="3" showAlt=true gap=5 height=300 >}}

