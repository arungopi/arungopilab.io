---
title: "Ripple effect"
date: 2021-12-31T00:00:00+05:30
draft: false
tags:
- photography
categories:
- gallery
---

# Turbulent and peaceful

"The mind is everything. What you think you become."

- Buddha

***



{{<gallery_row folder="images/" uniqueID="3" showAlt=true gap=5 height=300 >}}

