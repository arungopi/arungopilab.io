---
title: Who is this ?
author: 
disable_comments: true
---


# About Me

<!-- Centered Circular Profile Picture -->
<div style="text-align:center;">
  <img src="/img/latest.jpg" alt="Arun Gopinathan" width="150" height="150" style="border-radius: 50%;">
</div>

Hello, I'm Arun Gopinathan! An enthusiastic individual who finds joy in exploring new places, capturing moments through photography, and leveraging technology to contribute positively to nature.

I hold a background in Physics and Geographic Information System, always embracing the idea that learning is a thrilling journey.

## Current Roles

I currently work as a Technical Assistant, specializing in [Field Emission Scanning Electron Microscopy (FESEM) and Energy Dispersive X-ray Spectroscopy (EDS) at the Department of Physics, CUSAT](https://fesemdop.github.io/). Concurrently, I'm on the path to becoming a skilled data scientist, exploring the exciting realms of data analysis and machine learning.


## View My Resume

To learn more about my professional background, you can view my resume: [View Resume](https://gitlab.com/arungopi/arungopi.gitlab.io/uploads/6919f2c201ec0b137e03ff8a76ea330a/Arun_G_Resume.pdf)

## My Projects

Explore some of the projects I've worked on:

* [People in Space Right Now](https://arun-gopinath.shinyapps.io/People_space_now/): An R Shiny app listing astronauts currently in the ISS - [Source code](https://github.com/arungop/people_in_the_space_right_now). I have a [detailed blog post](https://arungopinathan.com/post/tutorials/2023-12-16-people-in-iss-now) on the same.


* [Beginner's Guide on Erdas Imagine](https://arungopi.gitlab.io/erdas/): Coauthored a beginner's cookbook on Erdas Imagine - [Source code](https://gitlab.com/arungopi/erdas)

* [Wedding Invitation Website](https://akhiarya.netlify.app): A responsive invitation site with a photo and video gallery - [Source code](https://github.com/arungop/akhiarya)


## Contact Me 

You can connect with me on [Github](https://github.com/arungop) and [Gitlab](https://gitlab.com/arungopi). Feel free to reach out via [e-mail](mailto:garunalways@gmail.com) for any inquiries. You can also find me on [Telegram](http://t.me/arungopinathan).







